import React from 'react';
import PropTypes from 'prop-types';

const Navbar = ({ icon, title }) => (
    <nav className="navbar bg-success">
        <h1>
            <i className={icon} /> {title}
        </h1>
    </nav>
);

Navbar.defaultProps = {
    title: 'News Api',
    icon: 'far fa-newspaper'
}

Navbar.propTypes ={
    title: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired
}

export default Navbar;