import {
    GET_NEWS,
    CREATE_NEWS,
    DELETE_NEWS,
    NEWS_LOADING
} from './types';

// Get News
export const getNews = id => dispatch => {
    dispatch(setNewsLoading);
    dispatch({ type: GET_NEWS, payload: id });
}

// Create News
export const addNews = newsData => dispatch => {
    dispatch({ type: CREATE_NEWS, payload: newsData });
}

// Delete News
export const deleteNews = id => dispatch => {
    dispatch({ type: DELETE_NEWS, payload: id });
}

// Set loading state
export const setNewsLoading = () => {
    return {
      type: NEWS_LOADING
    };
};