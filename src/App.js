import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import store from './store';

// Components
import Navbar from './components/layout/Navbar';
import AllNews from './components/news/AllNews';
import AddNews from './components/news/AddNews';
import SelectedNews from './components/news/SelectedNews';

import './App.css';

class App extends Component {
  render(){
    return(
      <Provider store={store}>
        <Router>
          <div className="App">
            <Navbar />
            <div className="container">
              <Route exact path="/" component={AllNews} />
              <Route path="/create" component={AddNews} />
              <Route path="/:id" component={SelectedNews} />
            </div>
          </div>
        </Router>
      </Provider>
    )
  }
}

export default App;
